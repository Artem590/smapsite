﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class ConfirmedEmails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Dictionary<string, string> d = new Dictionary<string, string>();


        var confirmedEmails = SpamApi.GetConfirmedEmailsWLinks();
        int countAll = SpamApi.GetCount();

        statisticsLable.Text = "Всего запросов: " + countAll.ToString() +
            ", активных (подтвержденных): " + confirmedEmails.Count.ToString() +
            ", ожидаются: " + (countAll - confirmedEmails.Count).ToString();

        foreach (var email in confirmedEmails)
        {
            var row = new TableRow();
            var cell1 = new TableCell();
            var cell2 = new TableCell();

            cell1.Text = email.Key;
            cell2.Text = email.Value;

            row.Cells.Add(cell1);
            row.Cells.Add(cell2);

            myTable.Rows.Add(row);
        }
    }
}