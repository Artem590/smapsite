﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Эдуард Хуснутдинов</h3>
    <address>
        https://vk.com/xitzer<br />
        <abbr title="Phone">P:</abbr>
        +7 927 966 44 98
    </address>
</asp:Content>
