﻿<%@ Page Title="Catch You" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="CatchYou.aspx.cs" Inherits="CatchYou" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Попался!</h1>
        <p class="lead">
            Ваш email адрес теперь добавлен в нашу базу данных, как активный.
        </p>
        <p><a href="/ConfirmedEmails" class="btn btn-primary btn-lg">Список активных адресов &raquo;</a></p>
    </div>
    <p><a href="/Default" class="btn btn-primary btn-lg">Главная &raquo;</a></p>
</asp:Content>
