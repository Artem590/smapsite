﻿using System;
using System.Collections.Generic;
//using System.Data.OleDb;
using System.Diagnostics;
using System.Data.SqlClient;

/// <summary>
/// Сводное описание для EmailDataBase
/// </summary>
public class EmailDataBase
{
    SqlConnectionStringBuilder builder;
    SqlConnection connection;
    string tableName = "emails";

    public EmailDataBase()
    {
        builder = new SqlConnectionStringBuilder();
        builder.DataSource = "emailentity.database.windows.net";
        builder.UserID = "svs590";
        builder.Password = "I1i2i3i4i5";
        builder.InitialCatalog = "EmailEntity";
        builder.PersistSecurityInfo = false;
        builder.MultipleActiveResultSets = false;
        builder.Encrypt = true;
        builder.TrustServerCertificate = false;
        builder.ConnectTimeout = 30;
    }

    public void Connect()
    {
        connection = new SqlConnection(builder.ConnectionString);
        connection.Open();
    }

    public List<string> GetConfirmedEmails()
    {
        List<string> confirmedEmails = new List<string>();

        string sql = @"SELECT Email FROM " + tableName + " WHERE Confirmed='1'";

        using (SqlCommand command = new SqlCommand(sql, connection))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    confirmedEmails.Add(reader.GetString(0));
                }
            }
        }

        return confirmedEmails;
    }

    public Dictionary<string, string> GetConfirmedEmailsWLinks()
    {
        Dictionary<string, string> confirmedEmailsWL = new Dictionary<string, string>();

        string sql = @"SELECT Email, Link FROM " + tableName + " WHERE Confirmed='1'";

        using (SqlCommand command = new SqlCommand(sql, connection))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    confirmedEmailsWL.Add(reader.GetString(0), reader.GetString(1));
                }
            }
        }

        return confirmedEmailsWL;
    }

    public void AddEmails(List<string> emails, List<string> links)
    {
        for (int i = 0; i < emails.Count; ++i)
        {
            try
            {
                string sql =
                    "INSERT INTO " + tableName + " (Email, Link, Confirmed, Name) " +
                       "VALUES ('" + emails[i] + "', '" + links[i] + "', '0', 'Name')";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
            // Игнорируем добавление email, которые уже добавлены
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }

    public void ConfirmEmail(string link)
    {
        string sql = "UPDATE " + tableName + " SET Confirmed = '1' WHERE Link = '" + link + "'";
        using (SqlCommand command = new SqlCommand(sql, connection))
        {
            command.ExecuteNonQuery();
        }
    }

    public int GetCount()
    {
        int result = 0;

        string sql = @"SELECT COUNT(Email) FROM " + tableName;

        using (SqlCommand command = new SqlCommand(sql, connection))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                result = reader.GetInt32(0);
            }
        }

        return result;
    }

    public void Close()
    {
        //connection.Close();
    }
}