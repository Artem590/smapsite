﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SpamSite.Startup))]
namespace SpamSite
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
