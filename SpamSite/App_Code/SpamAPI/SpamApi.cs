﻿using System.Collections.Generic;
using MimeKit;
using MailKit.Net.Smtp;
using System.Diagnostics;
using System;
using WebApplication1.Services;
using System.Web.Hosting;
using System.Web;

/// <summary>
/// Сводное описание для SpamApi
/// </summary>
public static class SpamApi
{
    static string serverName = "https://spamsiteeduardo.azurewebsites.net";

    public static async System.Threading.Tasks.Task AcceptEmailsAsync(List<string> emails)
    {
        List<string> links = new List<string>();

        foreach (var email in emails)
        {
            string encodedLink = System.Guid.NewGuid().ToString("N");
            links.Add(encodedLink);

            try
            {
                await EmailService.SendEmailAsync(email, "Работа", serverName + "/Confirmation.aspx?" + encodedLink);
                //await EmailService.SendEmailNoSmtpAsync(email, "Спам", serverName + "/Confirmation.aspx?" + encodedLink);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }


        EmailDataBase db = new EmailDataBase();
        db.Connect();
        db.AddEmails(emails, links);
        db.Close();
    }

    public static async System.Threading.Tasks.Task ConfirmEmail(string link)
    {
        EmailDataBase db = new EmailDataBase();
        db.Connect();

        try
        {
            db.ConfirmEmail(link);
        }
        // Пропускаем, если нет такой ссылки в базе
        catch (Exception e){

        }

        db.Close();
    }

    public static List<string> GetConfirmedEmails()
    {
        List<string> resut;

        EmailDataBase db = new EmailDataBase();
        db.Connect();
        resut = db.GetConfirmedEmails();
        db.Close();

        return resut;
    }

    public static Dictionary<string, string> GetConfirmedEmailsWLinks()
    {
        Debug.WriteLine("--- " + HostingEnvironment.ApplicationPhysicalPath);

        Dictionary<string, string> resut = new Dictionary<string, string>();
        Dictionary<string, string> preresut;

        EmailDataBase db = new EmailDataBase();
        db.Connect();
        preresut = db.GetConfirmedEmailsWLinks();

        foreach (var key in preresut.Keys)
        {
            resut.Add(key, serverName + "/Confirmation.aspx?" + preresut[key]);
        }

        db.Close();

        return resut;
    }

    public static int GetCount()
    {
        int resut;

        EmailDataBase db = new EmailDataBase();
        db.Connect();
        resut = db.GetCount();
        db.Close();

        return resut;
    }
}