﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mail;
using EASendMail;
using System.Diagnostics;
using System;

namespace WebApplication1.Services
{
    public static class EmailService
    {
        public static async Task SendEmailAsync(string email, string subject, string message)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.From = new System.Net.Mail.MailAddress("eduardosps@yandex.ru", "Эдуардо");

            // The important part -- configuring the SMTP client
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Port = 25;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mail.From.Address, "Qwe-1234");
            smtp.Host = "smtp.yandex.ru";

            //recipient address
            mail.To.Add(new System.Net.Mail.MailAddress(email));

            //Formatted mail body
            mail.IsBodyHtml = true;
            mail.Body = "Привет! Как дела? \n<a href=" + message + ">Смотри</a>";
            mail.Subject = subject;

            smtp.Send(mail);
        }

        public static async Task SendEmailNoSmtpAsync(string email, string subject, string message)
        {
            EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
            EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();

            // Set sender email address, please change it to yours
            oMail.From = "test@emailarchitect.net";

            // Set recipient email address, please change it to yours
            oMail.To = email;

            // Set email subject
            oMail.Subject = subject;

            // Set email body
            oMail.HtmlBody = "<a href=" + message + ">Перейдите по этой ссылке</a>";
            //oMail.TextBody = "<a href=" + message + ">Перейдите по этой ссылке</a>";

            // Set SMTP server address to "".
            SmtpServer oServer = new SmtpServer("");

            // Do not set user authentication
            // Do not set SSL connection

            try
            {
                oSmtp.SendMail(oServer, oMail);
            }
            catch (Exception ep)
            {
                Debug.WriteLine(ep.Message);
            }
        }
    }
}
