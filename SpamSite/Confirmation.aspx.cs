﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Confirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string uniqueLink = this.Context.Request.QueryString.ToString();

        if (uniqueLink.Length != 0)
        {
            Debug.WriteLine(uniqueLink);
            SpamApi.ConfirmEmail(uniqueLink);
        }

        this.Context.Response.Redirect("~/CatchYou");
    }
}