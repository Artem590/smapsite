﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Import Namespace=System.Diagnostics %>
<%@ Import Namespace=System.Text.RegularExpressions %>

<script runat="server" language="C#">
    void SubmitEmails(Object sender, EventArgs e)
    {
        List<string> emails = new List<string>();
        Regex emailRegex = new Regex(@"(.+?@[A-Za-z0-9]+\.[A-Za-z]+?)[\r\n ]");
        var matches = emailRegex.Matches(TextBox1.Text + '\n');
        if (matches.Count > 0)
        {
            foreach (Match email in matches)
            {
                emails.Add(email.Groups[1].Value);
            }

            SpamApi.AcceptEmailsAsync(emails);

            TextBox1.Text = "";
        }
    }
</script>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Spam Site Service</h1>
        <p class="lead">
            Данный сервис предоставляет базу активных email адресов. Добавьте список адресов в поле и нажмите кнопку Принять.
            Как только владелец адреса перейдет по уникальной ссылке, его адрес добавится в список активных email.
        </p>
        <p><a href="/ConfirmedEmails" class="btn btn-primary btn-lg">Список активных адресов &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Ввод адресов для проверки</h2>
            <p>
                Введите один или несколько email адресов в поле ниже. Каждый адрес должен начинаться с новой строки.
            </p>
            <p>
                <asp:TextBox ID="TextBox1" runat="server" Height="200px" TextMode="MultiLine" Width="994px"></asp:TextBox>
            </p>
            <p>
                <asp:Button ID="SubmitButton" Text="Принять" OnClick="SubmitEmails" runat="server"></asp:Button>
            </p>
        </div>

    </div>
</asp:Content>
